import React, { useState, useEffect } from 'react';
import {Container, Header, Title, Form, Item, Input, Button} from 'native-base';
import {View, Text, Image, Alert, AsyncStorage} from 'react-native';
import Homepage from './Homepage';
import ConnectedPage from './ConnectedPage';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

const App = () => {
    const [ token, setToken] = useState();
    useEffect(()=> {
        _retrieveData();
    }, [])

    const RootStack = createStackNavigator();

    const _retrieveData = async () => {
        try {
          const value = await AsyncStorage.getItem('token');
          if (value !== null) {
            // We have data!!
            setToken(value)
          }
        } catch (error) {
          // Error retrieving data
        }
      };

    return(
        <NavigationContainer>
            <RootStack.Navigator>
                {
                !token ? (
                    <>
                    <RootStack.Screen name="Home" component={Homepage}/>
                    </>
                ) : (
                    <>
                    <RootStack.Screen name="Connect" component={ConnectedPage}/>
                    </>
                )
                }
            </RootStack.Navigator>
        </NavigationContainer>
    );

};

export default App  ;