// import React, { useState } from 'react';
// import {Container, Header, Title, Form, Item, Input, Button} from 'native-base';
// import {View, Text, Image, Alert} from 'react-native';
// import { Login } from './api/authentication';

// const Homepage = () => {

    

//     return(
//         <Container>
//         <Header style={{backgroundColor:'blueviolet'}}>
//         <Title style={{color:'#fff', fontSize: 25 }}>Ecommerce Login</Title>
//         </Header>

//         <View style={{alignItems:'center', margin:20}}>
//         <Image source={require('./img/logo.jpg')} style={{width:300, height:250}} />
//         </View>
//         </Container>

//     );

// };

// export default Homepage ;

import React, { useState } from 'react';
import {Container, Header, Title, Form, Item, Input, Button} from 'native-base';
import {View, Text, Image, Alert, AsyncStorage} from 'react-native';
import { Login } from './api/authentication';


const Homepage = ({navigation}) => {
    console.log(navigation)
    const [email, setEmail] = useState('');
    const [emailError,setemailError] = useState('');

    //For Password
    const [password, setPassword] = useState('');
    const [passwordError,setpasswordError] = useState('');

    const signin = () => {

        if(email!="" && password!=""){
            const user={email,password};
            Login(user).then( async response => {

                Alert.alert("Thank you for sign in");
                console.log("Thank you for sign in",response)
                console.log('Response', response)
                try {
                    await AsyncStorage.setItem(
                      'token', response.token
                    );
                  } catch (error) {
                    // Error saving data
                  }
                navigation.navigate('Connect');

            });
        }

        if(email!=""){
            Alert.alert(email);
            setemailError('');
        }else{

            setemailError("Hey ! Email should not be empty")
        };

        if(password!=""){
            Alert.alert(password);
            setpasswordError('');
        }else{

            setpasswordError("Your password should not be empty")
        };
       
    }

    const forgot = () => {

        Alert.alert('Forgot Password');
    }






    return(
        <Container>
        <Header style={{backgroundColor:'blueviolet'}}>
        <Title style={{color:'#fff', fontSize: 25 }}>Ecommerce Login</Title>
        </Header>

        <View style={{alignItems:'center', margin:20}}>
        <Image source={require('./img/logo.jpg')} style={{width:300, height:250}} />
        </View>



        <Form style={{paddingLeft: 20, paddingRight: 20}}>
        <Item style={{marginTop:20}}>
        <Input placeholder="Email/Phone number"
        value={email}
        onChangeText={(email) => setEmail(email)}
        onChange={() => setemailError('')}
        />
        </Item>
        <Text style={{color:'red',marginLeft:20}}>{emailError}</Text>

        <Item style={{marginTop:20}}>
        <Input placeholder="Password"
        value={password}
        onChangeText={(password) => setPassword(password)}
        onChange={() => setpasswordError('')}
        />
        </Item>
        <Text style={{color:'blue'}}>Forgot ?</Text>
        <Text style={{color:'red',marginLeft:20}} onPress={forgot}>{passwordError}</Text>


        <Item style={{marginTop:20}}>
            <Button block style={{backgroundColor:'blueviolet', width:'100%'}} onPress={signin}>
            <Text style={{color:'#fff', fontSize:20, fontWeight:'bold'}} >Sign in</Text>
            </Button>


        </Item>

        </Form>
        </Container>

    );

};

export default Homepage  ;