import React, { useState } from 'react';
import {Container, Header, Title, Form, Item, Input, Button} from 'native-base';
import {View, Text, Image, Alert, AsyncStorage} from 'react-native';
import { Login } from './api/authentication';

const ConnectedPage = ({navigation}) => {

  const signout = async () => {
    await AsyncStorage.removeItem('token');
    navigation.navigate('Login')
  }

  return(
        <Container>
          <Text>Logged !!!</Text>
          <Button block style={{backgroundColor:'blueviolet', width:'100%'}}>
            <Text style={{color:'#fff', fontSize:20, fontWeight:'bold'}} onPress={signout}>Sign out</Text>
          </Button>
        </Container>

    );
};

export default ConnectedPage  ;